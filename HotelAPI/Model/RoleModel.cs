﻿using HotelCore.Repository.Entity;

namespace HotelAPI.Model
{
    public class RoleModel
    {
        public RoleType Title { get; set; }
        public string Description { get; set; }
    }
}
