﻿using log4net.Config;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Common.Logger
{
    public class Log4NetManager : ILoggerProvider
    {
        public Log4NetManager(string filename = "log4net.config")
        {
            var logRepository = log4net.LogManager.GetRepository(Assembly.GetEntryAssembly());
            var file = new FileInfo(filename);
            XmlConfigurator.Configure(logRepository, file);
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new Log4NetCore();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public virtual void Dispose(bool disposing)
        {
            //cleanup
        }
    }
}
