﻿using HotelCore.Repository.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Repository.Configuration
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");
            builder.HasKey(t => t.Id);
            builder.Property(t => t.FullName).IsRequired().HasMaxLength(150);
            builder.Property(t => t.Username).IsRequired().HasMaxLength(150);
            builder.Property(t => t.Password).IsRequired().HasMaxLength(150);
            builder.Property(t => t.Email).IsRequired().HasMaxLength(150);
            builder.Property(t => t.PhoneNumber).IsRequired().HasMaxLength(50);
            builder.Property(t => t.IsActive);
            builder.Property(t => t.DateBirth);
            builder.Property(t => t.ArrivalDate);
            builder.Property(t => t.DepartureDate);
            builder.Property(t => t.Gender).HasDefaultValue(false);
        }
    }
}
