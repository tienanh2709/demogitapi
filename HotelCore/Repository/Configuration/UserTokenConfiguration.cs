﻿using HotelCore.Repository.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Repository.Configuration
{
    public class UserTokenConfiguration : IEntityTypeConfiguration<UserToken>
    {
        public void Configure(EntityTypeBuilder<UserToken> builder)
        {
            builder.ToTable("UserToken");
            builder.HasKey(r => r.Id);
            builder.Property(r => r.UserId);
            builder.Property(r => r.VerifyToken);
            builder.Property(r => r.VerifyExpiryDate);
            builder.Property(r => r.ResetPasswordToken);
            builder.Property(r => r.ResetPassExpiryDate);
            builder.Property(r => r.RefreshToken);
            builder.Property(r => r.RefreshTokenExpiryDate);
        }
    }
}
