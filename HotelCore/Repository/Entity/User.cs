﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelCore.Repository.Entity
{
    public class User
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public bool Gender { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DateBirth { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Role Role { get; set; }
        public Guid RoleId { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }
        public bool IsActive { get; set; }
        public UserToken UserToken { get; set; }
    }
}
